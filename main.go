package main

import (
  "encoding/json"
  "log"
  "net/http"
  "github.com/gorilla/mux"
)

var people []Person

/*type Person struct {
	ID string `json:"id,omitempty"`
	FirstName string `json:"firstname,omitempty"`
	LastName string `json:"lastname,omitempty"`
	Address *Address `json:"address,omitempty"`
  }
  
  type Address struct {
	City string `json:"city,omitempty"`
	State string `json:"state,omitempty"`
  }*/

//Esta es la estructura como el cliente me va a enviar los datos
//Y como el servidor enviará a la BD
//Pero mas adelante cabron jaja

type Person struct {
	Id string        `json:"id,omitempty"`
	Fistname string  `json:"fistname,omitempty"`
	Lastname string  `json:"lastname,omitempty"`
	Address *Address `json:"address,omitempty"`
}

type Address struct{
	City string  `json:"city,omitempty"`
	State string `json:"state,omitempty"`
}

func getAllPeople(w http.ResponseWriter, req *http.Request){
	json.NewEncoder(w).Encode(people)
}
func findPerson(w http.ResponseWriter, req *http.Request){
	params := mux.Vars(req)
	for _, item := range people{
		if item.Id == params["id"] {
			json.NewEncoder(w).Encode(item)
			return
		}
	}
	json.NewEncoder(w).Encode(&Person{})
}

func createPerson(w http.ResponseWriter, req *http.Request){
	var person Person
	_ = json.NewDecoder(req.Body).Decode(&person)
	people = append(people, person)
	json.NewEncoder(w).Encode(people)
}

func deletePerson(w http.ResponseWriter, req *http.Request){
	params := mux.Vars(req)
	for index, item := range people{
		if item.Id == params["id"] {
			people = append(people[:index], people[index + 1:]...)
			return
		}
	}
	json.NewEncoder(w).Encode(people)
}

func updatePerson(w http.ResponseWriter, req *http.Request){}



func main(){
	router := mux.NewRouter()


	people = append(people, Person{ 
		Id: "1", 
		Fistname: "Travis", 
		Lastname: "Maddox", 
		Address: &Address{
			City:"Dubling", 
			State:"California"}, 
		})

	people = append(people, Person{ 
		Id: "2", 
		Fistname: "Trenton", 
		Lastname: "Maddox",  
		})

	//Rutas
	router.HandleFunc("/people", getAllPeople).Methods("GET")
	router.HandleFunc("/people/{id}", findPerson).Methods("GET")
	router.HandleFunc("/people", createPerson).Methods("POST")
	router.HandleFunc("/people", updatePerson).Methods("PUT")
	router.HandleFunc("/people/{id}", deletePerson).Methods("DELETE")

	log.Fatal(http.ListenAndServe(":4000", router))

} 